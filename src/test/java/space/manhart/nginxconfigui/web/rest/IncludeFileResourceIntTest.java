package space.manhart.nginxconfigui.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import space.manhart.nginxconfigui.NgConfigUiApp;
import space.manhart.nginxconfigui.domain.IncludeFile;
import space.manhart.nginxconfigui.repository.IncludeFileRepository;
import space.manhart.nginxconfigui.service.IncludeFileService;
import space.manhart.nginxconfigui.web.rest.IncludeFileResource;
import space.manhart.nginxconfigui.web.rest.errors.ExceptionTranslator;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static space.manhart.nginxconfigui.web.rest.TestUtil.createFormattingConversionService;

/**
 * Test class for the IncludeFileResource REST controller.
 *
 * @see IncludeFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NgConfigUiApp.class)
public class IncludeFileResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private IncludeFileRepository includeFileRepository;

    @Autowired
    private IncludeFileService includeFileService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIncludeFileMockMvc;

    private IncludeFile includeFile;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IncludeFileResource includeFileResource = new IncludeFileResource(includeFileService);
        this.restIncludeFileMockMvc = MockMvcBuilders.standaloneSetup(includeFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IncludeFile createEntity(EntityManager em) {
        IncludeFile includeFile = new IncludeFile()
            .name(DEFAULT_NAME)
            .value(DEFAULT_VALUE)
            .active(DEFAULT_ACTIVE);
        return includeFile;
    }

    @Before
    public void initTest() {
        includeFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createIncludeFile() throws Exception {
        int databaseSizeBeforeCreate = includeFileRepository.findAll().size();

        // Create the IncludeFile
        restIncludeFileMockMvc.perform(post("/api/include-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(includeFile)))
            .andExpect(status().isCreated());

        // Validate the IncludeFile in the database
        List<IncludeFile> includeFileList = includeFileRepository.findAll();
        assertThat(includeFileList).hasSize(databaseSizeBeforeCreate + 1);
        IncludeFile testIncludeFile = includeFileList.get(includeFileList.size() - 1);
        assertThat(testIncludeFile.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testIncludeFile.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testIncludeFile.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createIncludeFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = includeFileRepository.findAll().size();

        // Create the IncludeFile with an existing ID
        includeFile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIncludeFileMockMvc.perform(post("/api/include-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(includeFile)))
            .andExpect(status().isBadRequest());

        // Validate the IncludeFile in the database
        List<IncludeFile> includeFileList = includeFileRepository.findAll();
        assertThat(includeFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllIncludeFiles() throws Exception {
        // Initialize the database
        includeFileRepository.saveAndFlush(includeFile);

        // Get all the includeFileList
        restIncludeFileMockMvc.perform(get("/api/include-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(includeFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getIncludeFile() throws Exception {
        // Initialize the database
        includeFileRepository.saveAndFlush(includeFile);

        // Get the includeFile
        restIncludeFileMockMvc.perform(get("/api/include-files/{id}", includeFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(includeFile.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingIncludeFile() throws Exception {
        // Get the includeFile
        restIncludeFileMockMvc.perform(get("/api/include-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIncludeFile() throws Exception {
        // Initialize the database
        includeFileService.save(includeFile);

        int databaseSizeBeforeUpdate = includeFileRepository.findAll().size();

        // Update the includeFile
        IncludeFile updatedIncludeFile = includeFileRepository.findById(includeFile.getId()).get();
        // Disconnect from session so that the updates on updatedIncludeFile are not directly saved in db
        em.detach(updatedIncludeFile);
        updatedIncludeFile
            .name(UPDATED_NAME)
            .value(UPDATED_VALUE)
            .active(UPDATED_ACTIVE);

        restIncludeFileMockMvc.perform(put("/api/include-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIncludeFile)))
            .andExpect(status().isOk());

        // Validate the IncludeFile in the database
        List<IncludeFile> includeFileList = includeFileRepository.findAll();
        assertThat(includeFileList).hasSize(databaseSizeBeforeUpdate);
        IncludeFile testIncludeFile = includeFileList.get(includeFileList.size() - 1);
        assertThat(testIncludeFile.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testIncludeFile.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testIncludeFile.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingIncludeFile() throws Exception {
        int databaseSizeBeforeUpdate = includeFileRepository.findAll().size();

        // Create the IncludeFile

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIncludeFileMockMvc.perform(put("/api/include-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(includeFile)))
            .andExpect(status().isBadRequest());

        // Validate the IncludeFile in the database
        List<IncludeFile> includeFileList = includeFileRepository.findAll();
        assertThat(includeFileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIncludeFile() throws Exception {
        // Initialize the database
        includeFileService.save(includeFile);

        int databaseSizeBeforeDelete = includeFileRepository.findAll().size();

        // Delete the includeFile
        restIncludeFileMockMvc.perform(delete("/api/include-files/{id}", includeFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<IncludeFile> includeFileList = includeFileRepository.findAll();
        assertThat(includeFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IncludeFile.class);
        IncludeFile includeFile1 = new IncludeFile();
        includeFile1.setId(1L);
        IncludeFile includeFile2 = new IncludeFile();
        includeFile2.setId(includeFile1.getId());
        assertThat(includeFile1).isEqualTo(includeFile2);
        includeFile2.setId(2L);
        assertThat(includeFile1).isNotEqualTo(includeFile2);
        includeFile1.setId(null);
        assertThat(includeFile1).isNotEqualTo(includeFile2);
    }
}
