package space.manhart.nginxconfigui.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import space.manhart.nginxconfigui.NgConfigUiApp;
import space.manhart.nginxconfigui.domain.ConfigFile;
import space.manhart.nginxconfigui.repository.ConfigFileRepository;
import space.manhart.nginxconfigui.service.ConfigFileService;
import space.manhart.nginxconfigui.web.rest.ConfigFileResource;
import space.manhart.nginxconfigui.web.rest.errors.ExceptionTranslator;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static space.manhart.nginxconfigui.web.rest.TestUtil.createFormattingConversionService;

/**
 * Test class for the ConfigFileResource REST controller.
 *
 * @see ConfigFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NgConfigUiApp.class)
public class ConfigFileResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private ConfigFileRepository configFileRepository;

    @Autowired
    private ConfigFileService configFileService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restConfigFileMockMvc;

    private ConfigFile configFile;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConfigFileResource configFileResource = new ConfigFileResource(configFileService);
        this.restConfigFileMockMvc = MockMvcBuilders.standaloneSetup(configFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConfigFile createEntity(EntityManager em) {
        ConfigFile configFile = new ConfigFile()
            .name(DEFAULT_NAME)
            .value(DEFAULT_VALUE)
            .active(DEFAULT_ACTIVE);
        return configFile;
    }

    @Before
    public void initTest() {
        configFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createConfigFile() throws Exception {
        int databaseSizeBeforeCreate = configFileRepository.findAll().size();

        // Create the ConfigFile
        restConfigFileMockMvc.perform(post("/api/config-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configFile)))
            .andExpect(status().isCreated());

        // Validate the ConfigFile in the database
        List<ConfigFile> configFileList = configFileRepository.findAll();
        assertThat(configFileList).hasSize(databaseSizeBeforeCreate + 1);
        ConfigFile testConfigFile = configFileList.get(configFileList.size() - 1);
        assertThat(testConfigFile.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testConfigFile.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testConfigFile.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createConfigFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = configFileRepository.findAll().size();

        // Create the ConfigFile with an existing ID
        configFile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConfigFileMockMvc.perform(post("/api/config-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configFile)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigFile in the database
        List<ConfigFile> configFileList = configFileRepository.findAll();
        assertThat(configFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllConfigFiles() throws Exception {
        // Initialize the database
        configFileRepository.saveAndFlush(configFile);

        // Get all the configFileList
        restConfigFileMockMvc.perform(get("/api/config-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(configFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getConfigFile() throws Exception {
        // Initialize the database
        configFileRepository.saveAndFlush(configFile);

        // Get the configFile
        restConfigFileMockMvc.perform(get("/api/config-files/{id}", configFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(configFile.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingConfigFile() throws Exception {
        // Get the configFile
        restConfigFileMockMvc.perform(get("/api/config-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConfigFile() throws Exception {
        // Initialize the database
        configFileService.save(configFile);

        int databaseSizeBeforeUpdate = configFileRepository.findAll().size();

        // Update the configFile
        ConfigFile updatedConfigFile = configFileRepository.findById(configFile.getId()).get();
        // Disconnect from session so that the updates on updatedConfigFile are not directly saved in db
        em.detach(updatedConfigFile);
        updatedConfigFile
            .name(UPDATED_NAME)
            .value(UPDATED_VALUE)
            .active(UPDATED_ACTIVE);

        restConfigFileMockMvc.perform(put("/api/config-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedConfigFile)))
            .andExpect(status().isOk());

        // Validate the ConfigFile in the database
        List<ConfigFile> configFileList = configFileRepository.findAll();
        assertThat(configFileList).hasSize(databaseSizeBeforeUpdate);
        ConfigFile testConfigFile = configFileList.get(configFileList.size() - 1);
        assertThat(testConfigFile.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testConfigFile.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testConfigFile.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingConfigFile() throws Exception {
        int databaseSizeBeforeUpdate = configFileRepository.findAll().size();

        // Create the ConfigFile

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConfigFileMockMvc.perform(put("/api/config-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configFile)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigFile in the database
        List<ConfigFile> configFileList = configFileRepository.findAll();
        assertThat(configFileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteConfigFile() throws Exception {
        // Initialize the database
        configFileService.save(configFile);

        int databaseSizeBeforeDelete = configFileRepository.findAll().size();

        // Delete the configFile
        restConfigFileMockMvc.perform(delete("/api/config-files/{id}", configFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ConfigFile> configFileList = configFileRepository.findAll();
        assertThat(configFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigFile.class);
        ConfigFile configFile1 = new ConfigFile();
        configFile1.setId(1L);
        ConfigFile configFile2 = new ConfigFile();
        configFile2.setId(configFile1.getId());
        assertThat(configFile1).isEqualTo(configFile2);
        configFile2.setId(2L);
        assertThat(configFile1).isNotEqualTo(configFile2);
        configFile1.setId(null);
        assertThat(configFile1).isNotEqualTo(configFile2);
    }
}
