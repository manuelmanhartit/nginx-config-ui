/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { NgConfigUiTestModule } from '../../../test.module';
import { IncludeFileComponent } from 'app/entities/include-file/include-file.component';
import { IncludeFileService } from 'app/entities/include-file/include-file.service';
import { IncludeFile } from 'app/shared/model/include-file.model';

describe('Component Tests', () => {
    describe('IncludeFile Management Component', () => {
        let comp: IncludeFileComponent;
        let fixture: ComponentFixture<IncludeFileComponent>;
        let service: IncludeFileService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [NgConfigUiTestModule],
                declarations: [IncludeFileComponent],
                providers: []
            })
                .overrideTemplate(IncludeFileComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(IncludeFileComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IncludeFileService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new IncludeFile(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.includeFiles[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
