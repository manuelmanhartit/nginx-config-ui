/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NgConfigUiTestModule } from '../../../test.module';
import { IncludeFileDetailComponent } from 'app/entities/include-file/include-file-detail.component';
import { IncludeFile } from 'app/shared/model/include-file.model';

describe('Component Tests', () => {
    describe('IncludeFile Management Detail Component', () => {
        let comp: IncludeFileDetailComponent;
        let fixture: ComponentFixture<IncludeFileDetailComponent>;
        const route = ({ data: of({ includeFile: new IncludeFile(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [NgConfigUiTestModule],
                declarations: [IncludeFileDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(IncludeFileDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(IncludeFileDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.includeFile).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
