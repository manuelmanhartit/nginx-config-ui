/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { NgConfigUiTestModule } from '../../../test.module';
import { IncludeFileUpdateComponent } from 'app/entities/include-file/include-file-update.component';
import { IncludeFileService } from 'app/entities/include-file/include-file.service';
import { IncludeFile } from 'app/shared/model/include-file.model';

describe('Component Tests', () => {
    describe('IncludeFile Management Update Component', () => {
        let comp: IncludeFileUpdateComponent;
        let fixture: ComponentFixture<IncludeFileUpdateComponent>;
        let service: IncludeFileService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [NgConfigUiTestModule],
                declarations: [IncludeFileUpdateComponent]
            })
                .overrideTemplate(IncludeFileUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(IncludeFileUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IncludeFileService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new IncludeFile(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.includeFile = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new IncludeFile();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.includeFile = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
