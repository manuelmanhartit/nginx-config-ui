/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { NgConfigUiTestModule } from '../../../test.module';
import { IncludeFileDeleteDialogComponent } from 'app/entities/include-file/include-file-delete-dialog.component';
import { IncludeFileService } from 'app/entities/include-file/include-file.service';

describe('Component Tests', () => {
    describe('IncludeFile Management Delete Component', () => {
        let comp: IncludeFileDeleteDialogComponent;
        let fixture: ComponentFixture<IncludeFileDeleteDialogComponent>;
        let service: IncludeFileService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [NgConfigUiTestModule],
                declarations: [IncludeFileDeleteDialogComponent]
            })
                .overrideTemplate(IncludeFileDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(IncludeFileDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IncludeFileService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
