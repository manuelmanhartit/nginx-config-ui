/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { NgConfigUiTestModule } from '../../../test.module';
import { ConfigFileDeleteDialogComponent } from 'app/entities/config-file/config-file-delete-dialog.component';
import { ConfigFileService } from 'app/entities/config-file/config-file.service';

describe('Component Tests', () => {
    describe('ConfigFile Management Delete Component', () => {
        let comp: ConfigFileDeleteDialogComponent;
        let fixture: ComponentFixture<ConfigFileDeleteDialogComponent>;
        let service: ConfigFileService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [NgConfigUiTestModule],
                declarations: [ConfigFileDeleteDialogComponent]
            })
                .overrideTemplate(ConfigFileDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ConfigFileDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConfigFileService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
