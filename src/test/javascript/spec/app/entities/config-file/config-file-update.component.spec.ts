/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { NgConfigUiTestModule } from '../../../test.module';
import { ConfigFileUpdateComponent } from 'app/entities/config-file/config-file-update.component';
import { ConfigFileService } from 'app/entities/config-file/config-file.service';
import { ConfigFile } from 'app/shared/model/config-file.model';

describe('Component Tests', () => {
    describe('ConfigFile Management Update Component', () => {
        let comp: ConfigFileUpdateComponent;
        let fixture: ComponentFixture<ConfigFileUpdateComponent>;
        let service: ConfigFileService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [NgConfigUiTestModule],
                declarations: [ConfigFileUpdateComponent]
            })
                .overrideTemplate(ConfigFileUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ConfigFileUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConfigFileService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigFile(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configFile = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigFile();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configFile = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
