/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NgConfigUiTestModule } from '../../../test.module';
import { ConfigFileDetailComponent } from 'app/entities/config-file/config-file-detail.component';
import { ConfigFile } from 'app/shared/model/config-file.model';

describe('Component Tests', () => {
    describe('ConfigFile Management Detail Component', () => {
        let comp: ConfigFileDetailComponent;
        let fixture: ComponentFixture<ConfigFileDetailComponent>;
        const route = ({ data: of({ configFile: new ConfigFile(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [NgConfigUiTestModule],
                declarations: [ConfigFileDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ConfigFileDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ConfigFileDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.configFile).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
