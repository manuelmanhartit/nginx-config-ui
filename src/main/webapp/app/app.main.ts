import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ProdConfig } from './blocks/config/prod.config';
import { NgConfigUiAppModule } from './app.module';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/markdown/markdown';

ProdConfig();

if (module['hot']) {
    module['hot'].accept();
}



platformBrowserDynamic()
    .bootstrapModule(NgConfigUiAppModule, { preserveWhitespaces: true })
    .then(success => console.log(`Application started`))
    .catch(err => console.error(err));
