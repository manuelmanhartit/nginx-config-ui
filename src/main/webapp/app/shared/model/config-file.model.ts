import { ITemplate } from 'app/shared/model/template.model';

export interface IConfigFile {
    id?: number;
    name?: string;
    value?: any;
    active?: boolean;
    template?: ITemplate;
}

export class ConfigFile implements IConfigFile {
    constructor(public id?: number, public name?: string, public value?: any, public active?: boolean, public template?: ITemplate) {
        this.active = this.active || false;
    }
}
