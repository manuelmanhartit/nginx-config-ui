import { IConfigFile } from 'app/shared/model/config-file.model';

export interface ITemplate {
    id?: number;
    name?: string;
    value?: any;
    templates?: IConfigFile[];
}

export class Template implements ITemplate {
    constructor(public id?: number, public name?: string, public value?: any, public templates?: IConfigFile[]) {}
}
