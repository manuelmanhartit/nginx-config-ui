export interface IIncludeFile {
    id?: number;
    name?: string;
    value?: any;
    active?: boolean;
}

export class IncludeFile implements IIncludeFile {
    constructor(public id?: number, public name?: string, public value?: any, public active?: boolean) {
        this.active = this.active || false;
    }
}
