import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IncludeFile } from 'app/shared/model/include-file.model';
import { IncludeFileService } from './include-file.service';
import { IncludeFileComponent } from './include-file.component';
import { IncludeFileDetailComponent } from './include-file-detail.component';
import { IncludeFileUpdateComponent } from './include-file-update.component';
import { IncludeFileDeletePopupComponent } from './include-file-delete-dialog.component';
import { IIncludeFile } from 'app/shared/model/include-file.model';

@Injectable({ providedIn: 'root' })
export class IncludeFileResolve implements Resolve<IIncludeFile> {
    constructor(private service: IncludeFileService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IIncludeFile> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<IncludeFile>) => response.ok),
                map((includeFile: HttpResponse<IncludeFile>) => includeFile.body)
            );
        }
        return of(new IncludeFile());
    }
}

export const includeFileRoute: Routes = [
    {
        path: '',
        component: IncludeFileComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.includeFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: IncludeFileDetailComponent,
        resolve: {
            includeFile: IncludeFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.includeFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: IncludeFileUpdateComponent,
        resolve: {
            includeFile: IncludeFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.includeFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: IncludeFileUpdateComponent,
        resolve: {
            includeFile: IncludeFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.includeFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const includeFilePopupRoute: Routes = [
    {
        path: ':id/delete',
        component: IncludeFileDeletePopupComponent,
        resolve: {
            includeFile: IncludeFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.includeFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
