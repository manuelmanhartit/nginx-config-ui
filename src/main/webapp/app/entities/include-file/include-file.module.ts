import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { NgConfigUiSharedModule } from 'app/shared';
import {
    IncludeFileComponent,
    IncludeFileDetailComponent,
    IncludeFileUpdateComponent,
    IncludeFileDeletePopupComponent,
    IncludeFileDeleteDialogComponent,
    includeFileRoute,
    includeFilePopupRoute
} from './';

const ENTITY_STATES = [...includeFileRoute, ...includeFilePopupRoute];

@NgModule({
    imports: [NgConfigUiSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        IncludeFileComponent,
        IncludeFileDetailComponent,
        IncludeFileUpdateComponent,
        IncludeFileDeleteDialogComponent,
        IncludeFileDeletePopupComponent
    ],
    entryComponents: [IncludeFileComponent, IncludeFileUpdateComponent, IncludeFileDeleteDialogComponent, IncludeFileDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NgConfigUiIncludeFileModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
