import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IIncludeFile } from 'app/shared/model/include-file.model';
import { IncludeFileService } from './include-file.service';

@Component({
    selector: 'jhi-include-file-delete-dialog',
    templateUrl: './include-file-delete-dialog.component.html'
})
export class IncludeFileDeleteDialogComponent {
    includeFile: IIncludeFile;

    constructor(
        protected includeFileService: IncludeFileService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.includeFileService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'includeFileListModification',
                content: 'Deleted an includeFile'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-include-file-delete-popup',
    template: ''
})
export class IncludeFileDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ includeFile }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(IncludeFileDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.includeFile = includeFile;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/include-file', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/include-file', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
