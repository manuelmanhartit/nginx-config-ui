import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IIncludeFile } from 'app/shared/model/include-file.model';

@Component({
    selector: 'jhi-include-file-detail',
    templateUrl: './include-file-detail.component.html'
})
export class IncludeFileDetailComponent implements OnInit {
    includeFile: IIncludeFile;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ includeFile }) => {
            this.includeFile = includeFile;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
