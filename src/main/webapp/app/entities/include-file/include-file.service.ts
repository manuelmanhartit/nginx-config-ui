import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IIncludeFile } from 'app/shared/model/include-file.model';

type EntityResponseType = HttpResponse<IIncludeFile>;
type EntityArrayResponseType = HttpResponse<IIncludeFile[]>;

@Injectable({ providedIn: 'root' })
export class IncludeFileService {
    public resourceUrl = SERVER_API_URL + 'api/include-files';

    constructor(protected http: HttpClient) {}

    create(includeFile: IIncludeFile): Observable<EntityResponseType> {
        return this.http.post<IIncludeFile>(this.resourceUrl, includeFile, { observe: 'response' });
    }

    update(includeFile: IIncludeFile): Observable<EntityResponseType> {
        return this.http.put<IIncludeFile>(this.resourceUrl, includeFile, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IIncludeFile>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IIncludeFile[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
