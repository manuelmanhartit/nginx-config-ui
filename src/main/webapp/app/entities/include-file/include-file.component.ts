import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IIncludeFile } from 'app/shared/model/include-file.model';
import { AccountService } from 'app/core';
import { IncludeFileService } from './include-file.service';

@Component({
    selector: 'jhi-include-file',
    templateUrl: './include-file.component.html'
})
export class IncludeFileComponent implements OnInit, OnDestroy {
    includeFiles: IIncludeFile[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected includeFileService: IncludeFileService,
        protected jhiAlertService: JhiAlertService,
        protected dataUtils: JhiDataUtils,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.includeFileService
            .query()
            .pipe(
                filter((res: HttpResponse<IIncludeFile[]>) => res.ok),
                map((res: HttpResponse<IIncludeFile[]>) => res.body)
            )
            .subscribe(
                (res: IIncludeFile[]) => {
                    this.includeFiles = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInIncludeFiles();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IIncludeFile) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInIncludeFiles() {
        this.eventSubscriber = this.eventManager.subscribe('includeFileListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
