import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiDataUtils } from 'ng-jhipster';
import { IIncludeFile } from 'app/shared/model/include-file.model';
import { IncludeFileService } from './include-file.service';

@Component({
    selector: 'jhi-include-file-update',
    templateUrl: './include-file-update.component.html'
})
export class IncludeFileUpdateComponent implements OnInit {
    includeFile: IIncludeFile;
    isSaving: boolean;

    constructor(
        protected dataUtils: JhiDataUtils,
        protected includeFileService: IncludeFileService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ includeFile }) => {
            this.includeFile = includeFile;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.includeFile.id !== undefined) {
            this.subscribeToSaveResponse(this.includeFileService.update(this.includeFile));
        } else {
            this.subscribeToSaveResponse(this.includeFileService.create(this.includeFile));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IIncludeFile>>) {
        result.subscribe((res: HttpResponse<IIncludeFile>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
