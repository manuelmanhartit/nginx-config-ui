import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { NgConfigUiSharedModule } from 'app/shared';
import {
    ConfigFileComponent,
    ConfigFileDetailComponent,
    ConfigFileUpdateComponent,
    ConfigFileDeletePopupComponent,
    ConfigFileDeleteDialogComponent,
    configFileRoute,
    configFilePopupRoute
} from './';

const ENTITY_STATES = [...configFileRoute, ...configFilePopupRoute];

@NgModule({
    imports: [NgConfigUiSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ConfigFileComponent,
        ConfigFileDetailComponent,
        ConfigFileUpdateComponent,
        ConfigFileDeleteDialogComponent,
        ConfigFileDeletePopupComponent
    ],
    entryComponents: [ConfigFileComponent, ConfigFileUpdateComponent, ConfigFileDeleteDialogComponent, ConfigFileDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NgConfigUiConfigFileModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
