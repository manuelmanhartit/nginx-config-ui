import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IConfigFile } from 'app/shared/model/config-file.model';
import { ConfigFileService } from './config-file.service';
import { ITemplate } from 'app/shared/model/template.model';
import { TemplateService } from 'app/entities/template';

@Component({
    selector: 'jhi-config-file-update',
    templateUrl: './config-file-update.component.html'
})
export class ConfigFileUpdateComponent implements OnInit {
    configFile: IConfigFile;
    isSaving: boolean;

    templates: ITemplate[];

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected configFileService: ConfigFileService,
        protected templateService: TemplateService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ configFile }) => {
            this.configFile = configFile;
        });
        this.templateService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ITemplate[]>) => mayBeOk.ok),
                map((response: HttpResponse<ITemplate[]>) => response.body)
            )
            .subscribe((res: ITemplate[]) => (this.templates = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.configFile.id !== undefined) {
            this.subscribeToSaveResponse(this.configFileService.update(this.configFile));
        } else {
            this.subscribeToSaveResponse(this.configFileService.create(this.configFile));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IConfigFile>>) {
        result.subscribe((res: HttpResponse<IConfigFile>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTemplateById(index: number, item: ITemplate) {
        return item.id;
    }
}
