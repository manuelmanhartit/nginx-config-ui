import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IConfigFile } from 'app/shared/model/config-file.model';
import { ConfigFileService } from './config-file.service';

@Component({
    selector: 'jhi-config-file-delete-dialog',
    templateUrl: './config-file-delete-dialog.component.html'
})
export class ConfigFileDeleteDialogComponent {
    configFile: IConfigFile;

    constructor(
        protected configFileService: ConfigFileService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.configFileService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'configFileListModification',
                content: 'Deleted an configFile'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-config-file-delete-popup',
    template: ''
})
export class ConfigFileDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configFile }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ConfigFileDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.configFile = configFile;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/config-file', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/config-file', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
