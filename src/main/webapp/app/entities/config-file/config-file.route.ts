import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ConfigFile } from 'app/shared/model/config-file.model';
import { ConfigFileService } from './config-file.service';
import { ConfigFileComponent } from './config-file.component';
import { ConfigFileDetailComponent } from './config-file-detail.component';
import { ConfigFileUpdateComponent } from './config-file-update.component';
import { ConfigFileDeletePopupComponent } from './config-file-delete-dialog.component';
import { IConfigFile } from 'app/shared/model/config-file.model';

@Injectable({ providedIn: 'root' })
export class ConfigFileResolve implements Resolve<IConfigFile> {
    constructor(private service: ConfigFileService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IConfigFile> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<ConfigFile>) => response.ok),
                map((configFile: HttpResponse<ConfigFile>) => configFile.body)
            );
        }
        return of(new ConfigFile());
    }
}

export const configFileRoute: Routes = [
    {
        path: '',
        component: ConfigFileComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.configFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ConfigFileDetailComponent,
        resolve: {
            configFile: ConfigFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.configFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ConfigFileUpdateComponent,
        resolve: {
            configFile: ConfigFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.configFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ConfigFileUpdateComponent,
        resolve: {
            configFile: ConfigFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.configFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const configFilePopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ConfigFileDeletePopupComponent,
        resolve: {
            configFile: ConfigFileResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ngConfigUiApp.configFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
