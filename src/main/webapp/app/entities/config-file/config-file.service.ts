import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IConfigFile } from 'app/shared/model/config-file.model';

type EntityResponseType = HttpResponse<IConfigFile>;
type EntityArrayResponseType = HttpResponse<IConfigFile[]>;

@Injectable({ providedIn: 'root' })
export class ConfigFileService {
    public resourceUrl = SERVER_API_URL + 'api/config-files';

    constructor(protected http: HttpClient) {}

    create(configFile: IConfigFile): Observable<EntityResponseType> {
        return this.http.post<IConfigFile>(this.resourceUrl, configFile, { observe: 'response' });
    }

    sync(): Observable<String> {
        return this.http.post<String>(`${this.resourceUrl}/sync`, { observe: 'response' });
    }

    update(configFile: IConfigFile): Observable<EntityResponseType> {
        return this.http.put<IConfigFile>(this.resourceUrl, configFile, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IConfigFile>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IConfigFile[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
