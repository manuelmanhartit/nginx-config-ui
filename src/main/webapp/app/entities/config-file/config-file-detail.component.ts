import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IConfigFile } from 'app/shared/model/config-file.model';

@Component({
    selector: 'jhi-config-file-detail',
    templateUrl: './config-file-detail.component.html'
})
export class ConfigFileDetailComponent implements OnInit {
    configFile: IConfigFile;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configFile }) => {
            this.configFile = configFile;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
