import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'template',
                loadChildren: './template/template.module#NgConfigUiTemplateModule'
            },
            {
                path: 'include-file',
                loadChildren: './include-file/include-file.module#NgConfigUiIncludeFileModule'
            },
            {
                path: 'config-file',
                loadChildren: './config-file/config-file.module#NgConfigUiConfigFileModule'
            },
            {
                path: 'template',
                loadChildren: './template/template.module#NgConfigUiTemplateModule'
            },
            {
                path: 'config-file',
                loadChildren: './config-file/config-file.module#NgConfigUiConfigFileModule'
            },
            {
                path: 'template',
                loadChildren: './template/template.module#NgConfigUiTemplateModule'
            },
            {
                path: 'include-file',
                loadChildren: './include-file/include-file.module#NgConfigUiIncludeFileModule'
            },
            {
                path: 'config-file',
                loadChildren: './config-file/config-file.module#NgConfigUiConfigFileModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NgConfigUiEntityModule {}
