import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiDataUtils } from 'ng-jhipster';
import { ITemplate } from 'app/shared/model/template.model';
import { TemplateService } from './template.service';

@Component({
    selector: 'jhi-template-update',
    templateUrl: './template-update.component.html'
})
export class TemplateUpdateComponent implements OnInit {
    template: ITemplate;
    isSaving: boolean;

    constructor(protected dataUtils: JhiDataUtils, protected templateService: TemplateService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ template }) => {
            this.template = template;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.template.id !== undefined) {
            this.subscribeToSaveResponse(this.templateService.update(this.template));
        } else {
            this.subscribeToSaveResponse(this.templateService.create(this.template));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITemplate>>) {
        result.subscribe((res: HttpResponse<ITemplate>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
