package space.manhart.nginxconfigui.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import space.manhart.nginxconfigui.domain.Template;


/**
 * Spring Data  repository for the Template entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TemplateRepository extends JpaRepository<Template, Long> {

}
