package space.manhart.nginxconfigui.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import space.manhart.nginxconfigui.domain.ConfigFile;


/**
 * Spring Data  repository for the ConfigFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigFileRepository extends JpaRepository<ConfigFile, Long> {

}
