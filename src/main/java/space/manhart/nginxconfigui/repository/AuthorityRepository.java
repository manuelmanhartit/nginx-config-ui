package space.manhart.nginxconfigui.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import space.manhart.nginxconfigui.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
