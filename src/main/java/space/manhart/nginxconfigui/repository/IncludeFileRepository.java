package space.manhart.nginxconfigui.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import space.manhart.nginxconfigui.domain.IncludeFile;


/**
 * Spring Data  repository for the IncludeFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IncludeFileRepository extends JpaRepository<IncludeFile, Long> {

}
