package space.manhart.nginxconfigui.service;

import java.util.List;
import java.util.Optional;

import space.manhart.nginxconfigui.domain.IncludeFile;

/**
 * Service Interface for managing IncludeFile.
 */
public interface IncludeFileService {

    /**
     * Save a includeFile.
     *
     * @param includeFile the entity to save
     * @return the persisted entity
     */
    IncludeFile save(IncludeFile includeFile);

    /**
     * Get all the includeFiles.
     *
     * @return the list of entities
     */
    List<IncludeFile> findAll();


    /**
     * Get the "id" includeFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<IncludeFile> findOne(Long id);

    /**
     * Delete the "id" includeFile.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
