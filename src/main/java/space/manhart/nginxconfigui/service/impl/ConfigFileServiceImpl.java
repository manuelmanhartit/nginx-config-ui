package space.manhart.nginxconfigui.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import space.manhart.nginxconfigui.domain.ConfigFile;
import space.manhart.nginxconfigui.repository.ConfigFileRepository;
import space.manhart.nginxconfigui.service.ConfigFileService;
import space.manhart.nginxconfigui.service.ServiceConstants;
import space.manhart.nginxconfigui.service.util.ConfigFileConverterUtil;
import space.manhart.nginxconfigui.service.util.FileSystemAdapterUtil;
import space.manhart.nginxconfigui.service.util.FileSystemAdapterUtil.Result;

/**
 * Service Implementation for managing ConfigFile.
 */
@Service
@Transactional
public class ConfigFileServiceImpl implements ConfigFileService {

    private final Logger log = LoggerFactory.getLogger(ConfigFileServiceImpl.class);

    private final ConfigFileRepository configFileRepository;

    public ConfigFileServiceImpl(ConfigFileRepository configFileRepository) {
        this.configFileRepository = configFileRepository;
    }

    /**
     * Save a configFile.
     *
     * @param configFile the entity to save
     * @return the persisted entity
     */
    @Override
    public ConfigFile save(ConfigFile configFile) {
        log.debug("Request to save ConfigFile : {}", configFile);
        if (FileSystemAdapterUtil.writeFile(ServiceConstants.CONFIG_FILES_PATH, configFile, new ConfigFileConverterUtil())) {        	
        	return configFileRepository.save(configFile);
        }
        return null;
        // TODO throw exception
    }

    /**
     * Sync all entities from file system
     *
     * @return true if sync was successful
     */
    @Override
    @Transactional(readOnly = true)
    public String syncFromFileSystem() {
    	Result<ConfigFile> result = FileSystemAdapterUtil.readFiles(ServiceConstants.CONFIG_FILES_PATH, new ConfigFileConverterUtil());
		for (ConfigFile entity: result.entities) {
			save(entity);
		}
		return result.message;
    }

    /**
     * Get all the configFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ConfigFile> findAll(Pageable pageable) {
        log.debug("Request to get all ConfigFiles");
        return configFileRepository.findAll(pageable);
    }

    /**
     * Get one configFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ConfigFile> findOne(Long id) {
        log.debug("Request to get ConfigFile : {}", id);
        return configFileRepository.findById(id);
    }

    /**
     * Delete the configFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ConfigFile : {}", id);        configFileRepository.deleteById(id);
    }
}
