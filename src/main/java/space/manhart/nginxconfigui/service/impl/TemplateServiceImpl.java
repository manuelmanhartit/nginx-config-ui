package space.manhart.nginxconfigui.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import space.manhart.nginxconfigui.domain.Template;
import space.manhart.nginxconfigui.repository.TemplateRepository;
import space.manhart.nginxconfigui.service.TemplateService;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Template.
 */
@Service
@Transactional
public class TemplateServiceImpl implements TemplateService {

    private final Logger log = LoggerFactory.getLogger(TemplateServiceImpl.class);

    private final TemplateRepository templateRepository;

    public TemplateServiceImpl(TemplateRepository templateRepository) {
        this.templateRepository = templateRepository;
    }

    /**
     * Save a template.
     *
     * @param template the entity to save
     * @return the persisted entity
     */
    @Override
    public Template save(Template template) {
        log.debug("Request to save Template : {}", template);
        return templateRepository.save(template);
    }

    /**
     * Get all the templates.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Template> findAll() {
        log.debug("Request to get all Templates");
        return templateRepository.findAll();
    }


    /**
     * Get one template by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Template> findOne(Long id) {
        log.debug("Request to get Template : {}", id);
        return templateRepository.findById(id);
    }

    /**
     * Delete the template by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Template : {}", id);        templateRepository.deleteById(id);
    }
}
