package space.manhart.nginxconfigui.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import space.manhart.nginxconfigui.domain.IncludeFile;
import space.manhart.nginxconfigui.repository.IncludeFileRepository;
import space.manhart.nginxconfigui.service.IncludeFileService;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing IncludeFile.
 */
@Service
@Transactional
public class IncludeFileServiceImpl implements IncludeFileService {

    private final Logger log = LoggerFactory.getLogger(IncludeFileServiceImpl.class);

    private final IncludeFileRepository includeFileRepository;

    public IncludeFileServiceImpl(IncludeFileRepository includeFileRepository) {
        this.includeFileRepository = includeFileRepository;
    }

    /**
     * Save a includeFile.
     *
     * @param includeFile the entity to save
     * @return the persisted entity
     */
    @Override
    public IncludeFile save(IncludeFile includeFile) {
        log.debug("Request to save IncludeFile : {}", includeFile);
        return includeFileRepository.save(includeFile);
    }

    /**
     * Get all the includeFiles.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<IncludeFile> findAll() {
        log.debug("Request to get all IncludeFiles");
        return includeFileRepository.findAll();
    }


    /**
     * Get one includeFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<IncludeFile> findOne(Long id) {
        log.debug("Request to get IncludeFile : {}", id);
        return includeFileRepository.findById(id);
    }

    /**
     * Delete the includeFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete IncludeFile : {}", id);        includeFileRepository.deleteById(id);
    }
}
