package space.manhart.nginxconfigui.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import space.manhart.nginxconfigui.domain.ConfigFile;

import java.util.Optional;

/**
 * Service Interface for managing ConfigFile.
 */
public interface ConfigFileService {

    /**
     * Save a configFile.
     *
     * @param configFile the entity to save
     * @return the persisted entity
     */
    ConfigFile save(ConfigFile configFile);

    /**
     * Sync all entities from file system
     *
     * @return true if sync was successful
     */
    String syncFromFileSystem();

    /**
     * Get all the configFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ConfigFile> findAll(Pageable pageable);


    /**
     * Get the "id" configFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ConfigFile> findOne(Long id);

    /**
     * Delete the "id" configFile.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
