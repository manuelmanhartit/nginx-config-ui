package space.manhart.nginxconfigui.service.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import space.manhart.nginxconfigui.service.ServiceConstants;

public class FileSystemAdapterUtil {

	public static <T> boolean writeFile(String basePath, T entity, EntityConverterUtil<T> converter) {
		List<String> names = converter.getPossibleFileNames(entity);
		for (String name : names) {
			// move content into .old file
			File f = new File(basePath, name);
			if (f.exists()) {
				Optional<String> c = getFileContent(f);
				if (c.isPresent()) {
					// write backup file
					writeFile(Paths.get(f.getAbsolutePath() + ServiceConstants.BACKUP_FILE_POSTFIX), c.get());
					f.delete();
				}
			}
		}
		return writeFile(Paths.get(basePath, converter.getFileName(entity)), converter.getFileContent(entity));
	}
	
	private static boolean writeFile(Path path, String content) {
		try {
			Files.write(path, content.getBytes());
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static Optional<String> getFileContent(File file) {
		try {
			List<String> lines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
			String content = lines.stream().collect(Collectors.joining(System.getProperty("line.separator")));
			return Optional.of(content);
		} catch (IOException e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}


	public static class Result<T> {
		public String message;
		public List<T> entities;
		
		public Result() {
			entities = new ArrayList<>();
		}
	}
	
	public static <T> Result<T> readFiles(String filesPath, EntityConverterUtil<T> converter) {
		File path = new File(ServiceConstants.CONFIG_FILES_PATH);
		Result<T> result = new Result<>();
		
		if (!path.exists()) {
			result.message = "Path '" + path.getAbsolutePath() + "' does not exist.";
		} else {
			int iSuccess = 0, iFiles = 0;
			for (File file : path.listFiles(new FilesEndingFilter(true, ServiceConstants.BACKUP_FILE_POSTFIX))) {
				iFiles++;
				Optional<T> entity = converter.toEntity(file.getName(), getFileContent(file));
				if (entity.isPresent()) {
					result.entities.add(entity.get());
					iSuccess++;
				}
			}
			result.message = "I importet " + iSuccess + " of " + iFiles + " entities from the file system.";
		}
		return result;
	}

}
