package space.manhart.nginxconfigui.service.util;

import java.util.List;
import java.util.Optional;

public interface EntityConverterUtil<T> {
	public static String version = "0.1";
	public static String scmLink = "https://bitbucket.org/manuelmanhartit/nginx-config-ui/";
	public static String[] contentPrefixComment = new String[] {
		"# This file is edited with nginx-config-ui v" + version + ", please do not manually edit it",
		"# You can find nginx-config-ui at " + scmLink,
		// this should always be the last line, since we check for this line when stripping away the comments at the beginning
		"# You should not edit this file by hand since it will be overwritten by the tool"
	};

	public Optional<T> toEntity(String name, Optional<String> content);

	public List<String> getPossibleFileNames(T entity);

	public String getFileContent(T entity);

	public String getFileName(T entity);

}
