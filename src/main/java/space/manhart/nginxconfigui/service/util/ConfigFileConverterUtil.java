package space.manhart.nginxconfigui.service.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import space.manhart.nginxconfigui.domain.ConfigFile;
import space.manhart.nginxconfigui.service.ServiceConstants;

public class ConfigFileConverterUtil implements EntityConverterUtil<ConfigFile> {

	@Override
	public Optional<ConfigFile> toEntity(String name, Optional<String> content) {
		ConfigFile entity = new ConfigFile();		
		boolean active = _hasFilePostfix(name);
		entity.active(active);
		if (active) {
			entity.setName(_getFileNameWithoutPostfix(name));
		} else {
			entity.setName(name);
		}
		if (content.isPresent()) {
			entity.setValue(getContentWithoutAutoaddedRemark(content));
		} else {
			return Optional.empty();
		}
		return Optional.of(entity);
	}

	private String getContentWithoutAutoaddedRemark(Optional<String> content) {
		return null;
	}

	private String getAutoaddedRemark(Optional<String> content) {
		return null;
	}
	
	@Override
	public List<String> getPossibleFileNames(ConfigFile entity) {
		List<String> result = new ArrayList<>();
		result.add(_getFileName(entity));
		result.add(_getFileName(entity, true));
		return result;
	}

	@Override
	public String getFileContent(ConfigFile entity) {
		return getAutoAddPrefix() + entity.getValue();
	}

	private String getAutoAddPrefix() {
		return "";
	}

	@Override
	public String getFileName(ConfigFile entity) {
		return _getFileName(entity);
	}

	private boolean _hasFilePostfix(String fileName) {
		return fileName.endsWith(ServiceConstants.ACTIVE_POSTFIX);
	}
	
	private String _getFileNameWithoutPostfix(String fileName) {
		if (_hasFilePostfix(fileName)) {
			return fileName.substring(0, fileName.length() - ServiceConstants.ACTIVE_POSTFIX.length());
		}
		return fileName;
	}
	
	private String _getFileNameWithPostfix(String fileName) {
		if (!_hasFilePostfix(fileName)) {
			return fileName + ServiceConstants.ACTIVE_POSTFIX;
		}
		return fileName;
	}
	
	private String _getFileName(ConfigFile entity) {
		return _getFileName(entity, false);
	}

	private String _getFileName(ConfigFile entity, boolean invert) {
		String fileName = entity.getName();
		Boolean active = entity.isActive();
		if (invert) {
			active = !active;
		}
		if (active) {
			return _getFileNameWithPostfix(fileName); 
		} else {
			return _getFileNameWithoutPostfix(fileName); 
		}
	}

}
