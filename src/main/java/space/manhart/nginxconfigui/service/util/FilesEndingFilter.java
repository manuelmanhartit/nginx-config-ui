package space.manhart.nginxconfigui.service.util;

import java.io.File;
import java.io.FilenameFilter;

public class FilesEndingFilter implements FilenameFilter {

	private boolean blacklist;
	private String[] postfixes;

	public FilesEndingFilter(boolean blacklist, String... postfixes) {
		this.blacklist = blacklist;
		this.postfixes = postfixes;
	}

	@Override
	public boolean accept(File dir, String name) {
		boolean matches = false;
		for (String p : postfixes) {
			if (name.endsWith(p)) {
				matches = true;
			}
		}
		// if we have a blacklist filter, we must invert the matching
		if (blacklist) {
			matches = !matches;
		}
		// accept if file was found in postfix list
		return matches;
	}

}
