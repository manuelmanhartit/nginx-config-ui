package space.manhart.nginxconfigui.service;

/**
 * TODO find out how the jhipster way for configuration is and refactor to match that
 * @author manuel
 *
 */
public class ServiceConstants {
	public static final String ACTIVE_POSTFIX = ".conf";
	// the path where nginx configuration is stored
	public static final String BASE_CONFIG_PATH = "//home/jhipster/app/ngConfigUI/demo-config-path";
	// the path where nginx configuration is stored
	public static final String CONFIG_FILES_PATH = BASE_CONFIG_PATH + "/conf.d";
	// the path where nginx include files are stored
	public static final String INCLUDE_FILES_PATH = BASE_CONFIG_PATH + "/includes";
	public static final String BACKUP_FILE_POSTFIX = ".bak";
}
