package space.manhart.nginxconfigui.web.rest;
import io.github.jhipster.web.util.ResponseUtil;
import space.manhart.nginxconfigui.domain.IncludeFile;
import space.manhart.nginxconfigui.service.IncludeFileService;
import space.manhart.nginxconfigui.web.rest.errors.BadRequestAlertException;
import space.manhart.nginxconfigui.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing IncludeFile.
 */
@RestController
@RequestMapping("/api")
public class IncludeFileResource {

    private final Logger log = LoggerFactory.getLogger(IncludeFileResource.class);

    private static final String ENTITY_NAME = "includeFile";

    private final IncludeFileService includeFileService;

    public IncludeFileResource(IncludeFileService includeFileService) {
        this.includeFileService = includeFileService;
    }

    /**
     * POST  /include-files : Create a new includeFile.
     *
     * @param includeFile the includeFile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new includeFile, or with status 400 (Bad Request) if the includeFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/include-files")
    public ResponseEntity<IncludeFile> createIncludeFile(@RequestBody IncludeFile includeFile) throws URISyntaxException {
        log.debug("REST request to save IncludeFile : {}", includeFile);
        if (includeFile.getId() != null) {
            throw new BadRequestAlertException("A new includeFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IncludeFile result = includeFileService.save(includeFile);
        return ResponseEntity.created(new URI("/api/include-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /include-files : Updates an existing includeFile.
     *
     * @param includeFile the includeFile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated includeFile,
     * or with status 400 (Bad Request) if the includeFile is not valid,
     * or with status 500 (Internal Server Error) if the includeFile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/include-files")
    public ResponseEntity<IncludeFile> updateIncludeFile(@RequestBody IncludeFile includeFile) throws URISyntaxException {
        log.debug("REST request to update IncludeFile : {}", includeFile);
        if (includeFile.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IncludeFile result = includeFileService.save(includeFile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, includeFile.getId().toString()))
            .body(result);
    }

    /**
     * GET  /include-files : get all the includeFiles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of includeFiles in body
     */
    @GetMapping("/include-files")
    public List<IncludeFile> getAllIncludeFiles() {
        log.debug("REST request to get all IncludeFiles");
        return includeFileService.findAll();
    }

    /**
     * GET  /include-files/:id : get the "id" includeFile.
     *
     * @param id the id of the includeFile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the includeFile, or with status 404 (Not Found)
     */
    @GetMapping("/include-files/{id}")
    public ResponseEntity<IncludeFile> getIncludeFile(@PathVariable Long id) {
        log.debug("REST request to get IncludeFile : {}", id);
        Optional<IncludeFile> includeFile = includeFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(includeFile);
    }

    /**
     * DELETE  /include-files/:id : delete the "id" includeFile.
     *
     * @param id the id of the includeFile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/include-files/{id}")
    public ResponseEntity<Void> deleteIncludeFile(@PathVariable Long id) {
        log.debug("REST request to delete IncludeFile : {}", id);
        includeFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
