/**
 * View Models used by Spring MVC REST controllers.
 */
package space.manhart.nginxconfigui.web.rest.vm;
