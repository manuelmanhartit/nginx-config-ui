package space.manhart.nginxconfigui.web.rest;
import io.github.jhipster.web.util.ResponseUtil;
import space.manhart.nginxconfigui.domain.ConfigFile;
import space.manhart.nginxconfigui.service.ConfigFileService;
import space.manhart.nginxconfigui.web.rest.errors.BadRequestAlertException;
import space.manhart.nginxconfigui.web.rest.util.HeaderUtil;
import space.manhart.nginxconfigui.web.rest.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConfigFile.
 */
@RestController
@RequestMapping("/api")
public class ConfigFileResource {

    private final Logger log = LoggerFactory.getLogger(ConfigFileResource.class);

    private static final String ENTITY_NAME = "configFile";

    private final ConfigFileService configFileService;

    public ConfigFileResource(ConfigFileService configFileService) {
        this.configFileService = configFileService;
    }

    /**
     * POST  /config-files : Create a new configFile.
     *
     * @param configFile the configFile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new configFile, or with status 400 (Bad Request) if the configFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/config-files")
    public ResponseEntity<ConfigFile> createConfigFile(@RequestBody ConfigFile configFile) throws URISyntaxException {
        log.debug("REST request to save ConfigFile : {}", configFile);
        if (configFile.getId() != null) {
            throw new BadRequestAlertException("A new configFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConfigFile result = configFileService.save(configFile);
        return ResponseEntity.created(new URI("/api/config-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /config-files : Updates an existing configFile.
     *
     * @param configFile the configFile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated configFile,
     * or with status 400 (Bad Request) if the configFile is not valid,
     * or with status 500 (Internal Server Error) if the configFile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/config-files")
    public ResponseEntity<ConfigFile> updateConfigFile(@RequestBody ConfigFile configFile) throws URISyntaxException {
        log.debug("REST request to update ConfigFile : {}", configFile);
        if (configFile.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ConfigFile result = configFileService.save(configFile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, configFile.getId().toString()))
            .body(result);
    }

    /**
     * POST /config-files : Synchronizes files from file system
     *
     * @return the ResponseEntity with status 200 (OK) and with body the updated configFile,
     * or with status 400 (Bad Request) if the configFile is not valid,
     * or with status 500 (Internal Server Error) if the configFile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/config-files/sync")
    public ResponseEntity<String> synchronizeConfigFiles() throws URISyntaxException {
        log.debug("REST request to sync ConfigFiles from file system");
        String result = configFileService.syncFromFileSystem();
        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert(result, ""))
            .body(result);
    }

    /**
     * GET  /config-files : get all the configFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of configFiles in body
     */
    @GetMapping("/config-files")
    public ResponseEntity<List<ConfigFile>> getAllConfigFiles(Pageable pageable) {
        log.debug("REST request to get a page of ConfigFiles");
        Page<ConfigFile> page = configFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/config-files");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /config-files/:id : get the "id" configFile.
     *
     * @param id the id of the configFile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the configFile, or with status 404 (Not Found)
     */
    @GetMapping("/config-files/{id}")
    public ResponseEntity<ConfigFile> getConfigFile(@PathVariable Long id) {
        log.debug("REST request to get ConfigFile : {}", id);
        Optional<ConfigFile> configFile = configFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(configFile);
    }

    /**
     * DELETE  /config-files/:id : delete the "id" configFile.
     *
     * @param id the id of the configFile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/config-files/{id}")
    public ResponseEntity<Void> deleteConfigFile(@PathVariable Long id) {
        log.debug("REST request to delete ConfigFile : {}", id);
        configFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
